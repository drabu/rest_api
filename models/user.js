const mongoose      =   require('mongoose')
const bcrypt        =   require('bcrypt')


const UserSchema = mongoose.Schema({

    display_name : {
        type: String, 
        require : false
    },
    
    email : {
        type : String, 
        require : true
    },

    password : {
        type : String,
        require : true
    },

    username : {
        type : String,
        require : true
    }

})



const User = module.exports = mongoose.model('users', UserSchema)

module.exports.registerNewUser = ( newUser, callback )=>{
    
    bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newUser.password, salt, (err, hash) => {
            // Store hash in your password DB.
            newUser.password = hash
            newUser.save(callback)
                        
        });
    });

}



module.exports.isExistingUser = (userEmail, callback)=> {
    const query = {email:userEmail}
    User.findOne(query, callback)
}

module.exports.compareHashes = (candidatePassword, hash, callback)=>{
    bcrypt.compare( candidatePassword, hash, callback)

}