// external dependencies 
const express           =   require('express')
const bodyParser        =   require('body-parser')     
const mongoose          =   require('mongoose')


//internal dependencies
const users             =   require('./routes/user') 
const payments          =   require('./routes/payments')   
const config            =   require('./config/database')   
      


const app   = express();
const port  = 3000;

//database connectivity
mongoose.connect(config.database, {useNewUrlParser : true})

mongoose.connection.on('connected', ()=> {
    console.log('mongoose hase been connected')
})


//my app uses cases
app.use(bodyParser.json())
app.use('/users', users)
app.use('/payments', payments)

app.listen(port, () =>{
    console.log("Server started at port " + port)
})