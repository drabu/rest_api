const express       =   require('express')
const routes        =   express.Router();
const User          =   require('../models/user')

//public routes
routes.post('/registerUser', ( req, res)=> {
    console.log('we have a new user for registeration')

       User.isExistingUser(req.body.email, ( err, user )=>{
            
        if (user){
            res.json({success:false, msg: user.email + " alredy exists"})
        }else {

            const newUser = new User({

                email           : req.body.email,
                username        : req.body.username,
                password        : req.body.password,
                display_name    : req.body.display_name
        
            })
        
            User.registerNewUser(newUser, ( err, user )=> {

                if (err){
                    res.json({success:false, msg:"User was not registered due to an error!"})
                }else {
                    res.json({success:true, msg: user.email + " was registered successfully "})
        
                }
            
            })
        
        }

    })


})



routes.post('/authenticate', (req, res)=>{
    console.log('we have an authentication request')

    const userEmail         =   req.body.email
    const userPassword      =   req.body.password 

    User.isExistingUser(userEmail,(err, user)=>{

        if (err) throw err

        if (user){
            User.compareHashes(userPassword, user.password, (err, isMatching)=>{
                if (isMatching){
                    // print users id
                    res.json({success:true, msg:"User id to be created in token is " + user._id})
                }else {
                    res.json({success:false, msg:"Password mismatch with " + user.email})
                }
            })
        

        }else {
            res.json({success:false, msg:"No user found with " + userEmail})
            
        }

    })


})



//protected routes
routes.put('/updateProfile', ()=>{
    console.log('we have an update profile request')
})



 module.exports = routes